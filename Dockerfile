FROM jboss/wildfly
ADD /home/ubuntu/workspace/pipeline_demo/samplejar/target/samplejar-1.0-SNAPSHOT.jar  /opt/jboss/wildfly/standalone/deployments/
RUN /opt/jboss/wildfly/bin/add-user.sh admin admin --silent
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]

